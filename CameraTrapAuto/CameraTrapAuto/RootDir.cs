﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace CameraTrapAuto
{
    class RootDir
    {
        bool error;


        string rootDir;
        string enteredDir;
        string unenteredDir;
        

        int currSite;


        //these are all of the sites 
        List<SiteDir> siteDirs;

        public RootDir(string rootDirectory)
        {
            currSite = 0;


            error = false;
            siteDirs = new List<SiteDir>();

            //selected root directory
            rootDir = rootDirectory;

            //find entered
            if (Directory.Exists(rootDirectory + DirectoryTools.enteredStr))
            {
                enteredDir = rootDirectory + DirectoryTools.enteredStr;
            }
            else
            {
                error = true;
                MessageBox.Show("No entered directory found. Select the directory that contains the entered and unentered directories.");
                return;
            }

            //find unentered
            if (Directory.Exists(rootDirectory + DirectoryTools.unenteredStr))
            {
                unenteredDir = rootDirectory + DirectoryTools.unenteredStr;
            }
            else
            {
                error = true;
                MessageBox.Show("No unentered directory found. Select the directory that contains the entered and unentered directories.");
                return;
            }

            //create site directories
            foreach(string currDir in DirectoryTools.getDirectories(unenteredDir))
            {
                siteDirs.Add(new SiteDir(currDir));
            }



        }

        //try and get next video, returns false if we are at the end of the site
        public Videos nextVideo()
        {            
            return siteDirs[currSite].nextVideoChkRevisit();
            
        }

        public Videos getCurrentVideo()
        {
            return siteDirs[currSite].getCurrVideo();
        }
      
    }
}
