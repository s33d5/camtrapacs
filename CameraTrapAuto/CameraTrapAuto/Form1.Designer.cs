﻿namespace CameraTrapAuto
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.wmpVideoOutput = new AxWMPLib.AxWindowsMediaPlayer();
            this.lblInitials = new System.Windows.Forms.Label();
            this.txtbxInitOfUsr = new System.Windows.Forms.TextBox();
            this.txtbxSiteName = new System.Windows.Forms.TextBox();
            this.txtbxSiteNumber = new System.Windows.Forms.TextBox();
            this.txtbxRevDate = new System.Windows.Forms.TextBox();
            this.txtbcGenFileNm = new System.Windows.Forms.TextBox();
            this.lblSiteNm = new System.Windows.Forms.Label();
            this.lblRevDate = new System.Windows.Forms.Label();
            this.lblNewFileNm = new System.Windows.Forms.Label();
            this.lblSiteID = new System.Windows.Forms.Label();
            this.btnDirSel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbxVidDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbxDuration = new System.Windows.Forms.TextBox();
            this.lblCurrDir = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxSiteID = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.cmbBxResCamStart = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblMrkEntered = new System.Windows.Forms.Label();
            this.chkBxMarkEntered = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.wmpVideoOutput)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // wmpVideoOutput
            // 
            this.wmpVideoOutput.Enabled = true;
            this.wmpVideoOutput.Location = new System.Drawing.Point(12, 25);
            this.wmpVideoOutput.Name = "wmpVideoOutput";
            this.wmpVideoOutput.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmpVideoOutput.OcxState")));
            this.wmpVideoOutput.Size = new System.Drawing.Size(669, 301);
            this.wmpVideoOutput.TabIndex = 0;
            // 
            // lblInitials
            // 
            this.lblInitials.AutoSize = true;
            this.lblInitials.Location = new System.Drawing.Point(39, 334);
            this.lblInitials.Name = "lblInitials";
            this.lblInitials.Size = new System.Drawing.Size(74, 13);
            this.lblInitials.TabIndex = 1;
            this.lblInitials.Text = "Initials of user:";
            // 
            // txtbxInitOfUsr
            // 
            this.txtbxInitOfUsr.Location = new System.Drawing.Point(116, 332);
            this.txtbxInitOfUsr.Name = "txtbxInitOfUsr";
            this.txtbxInitOfUsr.Size = new System.Drawing.Size(33, 20);
            this.txtbxInitOfUsr.TabIndex = 2;
            // 
            // txtbxSiteName
            // 
            this.txtbxSiteName.Location = new System.Drawing.Point(72, 367);
            this.txtbxSiteName.Name = "txtbxSiteName";
            this.txtbxSiteName.Size = new System.Drawing.Size(111, 20);
            this.txtbxSiteName.TabIndex = 3;
            // 
            // txtbxSiteNumber
            // 
            this.txtbxSiteNumber.Location = new System.Drawing.Point(72, 393);
            this.txtbxSiteNumber.Name = "txtbxSiteNumber";
            this.txtbxSiteNumber.Size = new System.Drawing.Size(111, 20);
            this.txtbxSiteNumber.TabIndex = 4;
            // 
            // txtbxRevDate
            // 
            this.txtbxRevDate.Location = new System.Drawing.Point(350, 367);
            this.txtbxRevDate.Name = "txtbxRevDate";
            this.txtbxRevDate.Size = new System.Drawing.Size(111, 20);
            this.txtbxRevDate.TabIndex = 5;
            // 
            // txtbcGenFileNm
            // 
            this.txtbcGenFileNm.Location = new System.Drawing.Point(350, 393);
            this.txtbcGenFileNm.Name = "txtbcGenFileNm";
            this.txtbcGenFileNm.Size = new System.Drawing.Size(111, 20);
            this.txtbcGenFileNm.TabIndex = 6;
            // 
            // lblSiteNm
            // 
            this.lblSiteNm.AutoSize = true;
            this.lblSiteNm.Location = new System.Drawing.Point(12, 370);
            this.lblSiteNm.Name = "lblSiteNm";
            this.lblSiteNm.Size = new System.Drawing.Size(57, 13);
            this.lblSiteNm.TabIndex = 7;
            this.lblSiteNm.Text = "Site name:";
            // 
            // lblRevDate
            // 
            this.lblRevDate.AutoSize = true;
            this.lblRevDate.Location = new System.Drawing.Point(211, 370);
            this.lblRevDate.Name = "lblRevDate";
            this.lblRevDate.Size = new System.Drawing.Size(133, 13);
            this.lblRevDate.TabIndex = 8;
            this.lblRevDate.Text = "Revisit date (dd/mm/yyyy):";
            // 
            // lblNewFileNm
            // 
            this.lblNewFileNm.AutoSize = true;
            this.lblNewFileNm.Location = new System.Drawing.Point(267, 400);
            this.lblNewFileNm.Name = "lblNewFileNm";
            this.lblNewFileNm.Size = new System.Drawing.Size(77, 13);
            this.lblNewFileNm.TabIndex = 9;
            this.lblNewFileNm.Text = "New file name:";
            // 
            // lblSiteID
            // 
            this.lblSiteID.AutoSize = true;
            this.lblSiteID.Location = new System.Drawing.Point(1, 396);
            this.lblSiteID.Name = "lblSiteID";
            this.lblSiteID.Size = new System.Drawing.Size(68, 13);
            this.lblSiteID.TabIndex = 10;
            this.lblSiteID.Text = "Site Number:";
            // 
            // btnDirSel
            // 
            this.btnDirSel.Location = new System.Drawing.Point(443, 333);
            this.btnDirSel.Name = "btnDirSel";
            this.btnDirSel.Size = new System.Drawing.Size(314, 23);
            this.btnDirSel.TabIndex = 11;
            this.btnDirSel.Text = "Select parent directory containing entered and unentered";
            this.btnDirSel.UseVisualStyleBackColor = true;
            this.btnDirSel.Click += new System.EventHandler(this.btnDirSel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(467, 374);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Video date (dd/mm/yyyy):";
            // 
            // txtbxVidDate
            // 
            this.txtbxVidDate.Location = new System.Drawing.Point(601, 371);
            this.txtbxVidDate.Name = "txtbxVidDate";
            this.txtbxVidDate.Size = new System.Drawing.Size(111, 20);
            this.txtbxVidDate.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(467, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Time of video start (24 hr):";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(601, 393);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(111, 20);
            this.textBox1.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(498, 424);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Duration (hh:mm:ss):";
            // 
            // txtbxDuration
            // 
            this.txtbxDuration.Location = new System.Drawing.Point(603, 421);
            this.txtbxDuration.Name = "txtbxDuration";
            this.txtbxDuration.Size = new System.Drawing.Size(111, 20);
            this.txtbxDuration.TabIndex = 16;
            // 
            // lblCurrDir
            // 
            this.lblCurrDir.AutoSize = true;
            this.lblCurrDir.Location = new System.Drawing.Point(13, 6);
            this.lblCurrDir.Name = "lblCurrDir";
            this.lblCurrDir.Size = new System.Drawing.Size(95, 13);
            this.lblCurrDir.TabIndex = 18;
            this.lblCurrDir.Text = "Selected directory:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(687, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Next video";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(687, 260);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Previous video";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 421);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Site ID:";
            // 
            // txtBoxSiteID
            // 
            this.txtBoxSiteID.Location = new System.Drawing.Point(72, 418);
            this.txtBoxSiteID.Name = "txtBoxSiteID";
            this.txtBoxSiteID.Size = new System.Drawing.Size(111, 20);
            this.txtBoxSiteID.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Controls.Add(this.cmbBxResCamStart);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Location = new System.Drawing.Point(4, 444);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 88);
            this.panel1.TabIndex = 23;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 2;
            this.trackBar1.Location = new System.Drawing.Point(266, 10);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(384, 45);
            this.trackBar1.TabIndex = 31;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // cmbBxResCamStart
            // 
            this.cmbBxResCamStart.FormattingEnabled = true;
            this.cmbBxResCamStart.Items.AddRange(new object[] {
            "Y",
            "N",
            "N/A"});
            this.cmbBxResCamStart.Location = new System.Drawing.Point(158, 36);
            this.cmbBxResCamStart.Name = "cmbBxResCamStart";
            this.cmbBxResCamStart.Size = new System.Drawing.Size(33, 21);
            this.cmbBxResCamStart.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Researcher cam handle start?";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Non-researcher human count:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(158, 7);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(33, 20);
            this.textBox2.TabIndex = 24;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lblMrkEntered
            // 
            this.lblMrkEntered.AutoSize = true;
            this.lblMrkEntered.Location = new System.Drawing.Point(684, 25);
            this.lblMrkEntered.Name = "lblMrkEntered";
            this.lblMrkEntered.Size = new System.Drawing.Size(80, 13);
            this.lblMrkEntered.TabIndex = 25;
            this.lblMrkEntered.Text = "Mark complete:";
            // 
            // chkBxMarkEntered
            // 
            this.chkBxMarkEntered.AutoSize = true;
            this.chkBxMarkEntered.Location = new System.Drawing.Point(768, 25);
            this.chkBxMarkEntered.Name = "chkBxMarkEntered";
            this.chkBxMarkEntered.Size = new System.Drawing.Size(15, 14);
            this.chkBxMarkEntered.TabIndex = 24;
            this.chkBxMarkEntered.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(795, 544);
            this.Controls.Add(this.lblMrkEntered);
            this.Controls.Add(this.chkBxMarkEntered);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBoxSiteID);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblCurrDir);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtbxDuration);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbxVidDate);
            this.Controls.Add(this.btnDirSel);
            this.Controls.Add(this.lblSiteID);
            this.Controls.Add(this.lblNewFileNm);
            this.Controls.Add(this.lblRevDate);
            this.Controls.Add(this.lblSiteNm);
            this.Controls.Add(this.txtbcGenFileNm);
            this.Controls.Add(this.txtbxRevDate);
            this.Controls.Add(this.txtbxSiteNumber);
            this.Controls.Add(this.txtbxSiteName);
            this.Controls.Add(this.txtbxInitOfUsr);
            this.Controls.Add(this.lblInitials);
            this.Controls.Add(this.wmpVideoOutput);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.wmpVideoOutput)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer wmpVideoOutput;
        private System.Windows.Forms.Label lblInitials;
        private System.Windows.Forms.TextBox txtbxInitOfUsr;
        private System.Windows.Forms.TextBox txtbxSiteName;
        private System.Windows.Forms.TextBox txtbxSiteNumber;
        private System.Windows.Forms.TextBox txtbxRevDate;
        private System.Windows.Forms.TextBox txtbcGenFileNm;
        private System.Windows.Forms.Label lblSiteNm;
        private System.Windows.Forms.Label lblRevDate;
        private System.Windows.Forms.Label lblNewFileNm;
        private System.Windows.Forms.Label lblSiteID;
        private System.Windows.Forms.Button btnDirSel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbxVidDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbxDuration;
        private System.Windows.Forms.Label lblCurrDir;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxSiteID;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblMrkEntered;
        private System.Windows.Forms.CheckBox chkBxMarkEntered;
        private System.Windows.Forms.ComboBox cmbBxResCamStart;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Timer timer1;
    }
}

