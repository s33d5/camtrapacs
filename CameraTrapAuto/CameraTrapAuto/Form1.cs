﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CameraTrapAuto
{
    public partial class Form1 : Form
    {
        RootDir rootDir;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDirSel_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();

            lblCurrDir.Text = folderBrowserDialog.SelectedPath;
            //create a new root directory
            rootDir = new RootDir(folderBrowserDialog.SelectedPath);
            playFile(rootDir.getCurrentVideo().dir);


        }

        void playFile(string videoURL)
        {
            
            wmpVideoOutput.URL = videoURL;
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Enabled = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private void timer1_Tick(object Sender, EventArgs e)
        {
            // Set the caption to the current time.  
            if ((int)(Math.Truncate((wmpVideoOutput.Ctlcontrols.currentPosition / wmpVideoOutput.currentMedia.duration) * 100)) >= 0) 
                trackBar1.Value = (int)(Math.Truncate((wmpVideoOutput.Ctlcontrols.currentPosition / wmpVideoOutput.currentMedia.duration) * 100));
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string temp = rootDir.nextVideo().dir;
            Console.WriteLine(temp);
            if (temp != "NO VIDEO")
            {
                playFile(temp);
            }
            else
            {
                MessageBox.Show("Reached end of videos for this site");
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            wmpVideoOutput.Ctlcontrols.currentPosition = (wmpVideoOutput.currentMedia.duration * trackBar1.Value)/100;
           

        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {

        }
    }
}
