IronOcr  - The Tesseract 5 OCR Library for .NET 

Supports applications and websites developed in 
- .Net FrameWork 4 (and above) for Windows, Linux Mono, MacOs Xamarin + Mono, Docker and Azure
- .Net Standard & Core 2 & 3.x for Windows, Linux, MacOs, Docker and Azure

Please visit https://ironsoftware.com/csharp/ocr/ for:
- Code Samples,
- Support,
- MSDN object reference,
- Detailed Community Tutorials.
 
