﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace CamProto
{
    public partial class MainMenu : Form
    {
        public static string mainDirectory;
        public static string excelFile;
        public MainMenu()
        {
            InitializeComponent();
            mainDirectory = "";
            excelFile = "";
        }

        private void btnStrtEntering_Click(object sender, EventArgs e)
        {
            

            if (mainDirectory != "" && excelFile != "")
            {
                var navVids = new NavigateVideos(mainDirectory + "\\unentered");
                if (navVids.getCurrentVideo() != "-1")
                {
                    var newVidInpMenu = new VideoInputMenu();

                    //check access to excel and videos
                    //backup excel file
                    try
                    {
                        var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\" + DateTime.Now.ToString("MM-dd-yyyy HH-mm-ss") + ".xlsx";
                        File.Copy(excelFile, path);
                        MessageBox.Show("Created excel backup to: " + Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\" + DateTime.Now + ".xlsx");
                    }
                    catch (Exception eee)
                    {
                        MessageBox.Show("Could not create excel backup, do this manually. Error: " + eee.Message);
                    }

                    MessageBox.Show("Make sure that the excel file that you are using is NOT open in Excel. Excel locks the file when it is open and means that this program will not be able to edit it.");

                    newVidInpMenu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Can't find any unentered video files.");
                }
            }
            else
            {
                MessageBox.Show("Select main directory and excel file");
            }
        }

        bool checkValidTopFolder(string path)
        {
            if (path == "")
            {
                MessageBox.Show("Not a valid path.");
                return false;
            }

            if (Directory.Exists(path + "\\Unentered") == true || Directory.Exists(path + "\\unentered") == true)
            {

            }
            else
            {
                MessageBox.Show("Selected folder requires a subfolder named unentered.");
                return false;
            }

            return true;
        }

        void listAllVideos(string directory)
        {
            var navVids = new NavigateVideos(directory + "\\unentered");

            foreach (var file in navVids.arrOfVideos())
            {
                listBoxUnenteredVideos.Items.Add(file);                
            }
        }
        private void btnSelOverallDirectory_Click(object sender, EventArgs e)
        {
            listBoxUnenteredVideos.Items.Clear();
            //check if top directory has entered subfolder
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();
            
            if(checkValidTopFolder(folderBrowserDialog.SelectedPath) == true)
            {
                mainDirectory = folderBrowserDialog.SelectedPath;
                lblCurrDir.Text = mainDirectory;
                listAllVideos(mainDirectory);
            }

        }

        private void btnSelectExcelFile_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                }
            }

            //lblSelExcelFile.Text = filePath;
            excelFile = filePath;
            lblSelExcelFile.Text = excelFile;
        }
    }
}
