﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.IO;
namespace CamProto
{


    public class FFMPEG
    {
        Process ffmpeg;
        
        public void exec(string input, string output)
        {
            char quote = '"';

            if (File.Exists(output))
            {
                File.Delete(output);
            }

            ffmpeg = new Process();

            ffmpeg.StartInfo.Arguments = " -i " + quote + input + quote + " -vframes 1 " + output;
            ffmpeg.StartInfo.FileName = "utils/ffmpeg.exe";
            ffmpeg.StartInfo.UseShellExecute = false;
            ffmpeg.StartInfo.RedirectStandardOutput = true;
            ffmpeg.StartInfo.RedirectStandardError = true;
            ffmpeg.StartInfo.CreateNoWindow = true;

            ffmpeg.Start();
            ffmpeg.WaitForExit();
            ffmpeg.Close();



        }


        public void GetThumbnail(string video, string jpg, string velicina)
        {
            if (velicina == null) velicina = "640x480";
            exec(video, jpg);
        }
    }
}
