﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using Tesseract;
namespace CamProto
{
    class NavigateVideos
    {

        List<string> allVideoPaths;
        
        //get all videos in dir and subdir
        public NavigateVideos(string topDirectory)
        {
            allVideoPaths = new List<string>();
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(topDirectory);
            FileInfo[] allFiles = objDirectoryInfo.GetFiles("*.avi", SearchOption.AllDirectories);

            foreach (var file in allFiles)
            {
                allVideoPaths.Add(file.FullName);
            }
        }

        public string currRevisitDate()
        {
            string revDateVal = "";
            try
            {
                string revString = "\\Revisit Date - ";
                int indOfRevisitDate = allVideoPaths[0].IndexOf(revString, StringComparison.OrdinalIgnoreCase);
                string revDateAndFileName = allVideoPaths[0].Substring(indOfRevisitDate + revString.Length, allVideoPaths[0].Length - (indOfRevisitDate + revString.Length));
                int indOfFileName = revDateAndFileName.IndexOf("\\");
                string revDate = revDateAndFileName.Substring(0, indOfFileName);

                revDateVal = Convert.ToDateTime(revDate).ToString("dd/MM/yyyy").Replace("-", "/");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

            return revDateVal;
        }

        public string currSiteNumber()
        {
            string siteNo = "";

            try
            {
                string unenteredStr = "\\Unentered";
                //get the indes of the start of unentered
                int indOfUnentered = allVideoPaths[0].IndexOf(unenteredStr, StringComparison.OrdinalIgnoreCase);
                //extract all of string passed unentered leaving the id, site name, revisit date, and file name
                string IDSiteAndRevisit = allVideoPaths[0].Substring(indOfUnentered + unenteredStr.Length, allVideoPaths[0].Length - (indOfUnentered + unenteredStr.Length));

                //get the space between the id and site name
                int indOfSiteName = IDSiteAndRevisit.IndexOf(" ");

                siteNo = IDSiteAndRevisit.Substring(1, indOfSiteName - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

            return siteNo;
        }

        public string currSiteName()
        {
            string siteName = "";
            try
            {
                string unenteredStr = "\\Unentered";
                //get the indes of the start of unentered
                int indOfUnentered = allVideoPaths[0].IndexOf(unenteredStr, StringComparison.OrdinalIgnoreCase);
                //extract all of string passed unentered leaving the id, site name, revisit date, and file name
                string IDSiteAndRevisit = allVideoPaths[0].Substring(indOfUnentered + unenteredStr.Length, allVideoPaths[0].Length - (indOfUnentered + unenteredStr.Length));

                //get the space between the id and site name
                int indOfSiteName = IDSiteAndRevisit.IndexOf(" ");
                //extract all of string passed the id leaving site name, revisit date, and file name
                string siteAndRevisit = IDSiteAndRevisit.Substring(indOfSiteName, IDSiteAndRevisit.Length - indOfSiteName);

                //get position of \\ to get to the revisit date
                int indOfRevDate = siteAndRevisit.IndexOf("\\");
                //get from the start to the slash
                siteName = siteAndRevisit.Substring(1, indOfRevDate - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

            return siteName;
        }

        public string currSiteID()
        {
            string year = "";
            try
            {
                string currRevDate = currRevisitDate();
                int indOfLastSlash = currRevDate.LastIndexOf("/");
                year = currRevDate.Substring(indOfLastSlash + 1, (currRevDate.Length - indOfLastSlash) - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }
            return year + currSiteNumber();
        }

        public string currFileName()
        {
            string newFileName = "";
            string fileName = "";
            string numberAndAVI = "";
            try
            {
                int indOfLastSlash = allVideoPaths[0].LastIndexOf("\\");
                fileName = allVideoPaths[0].Substring(indOfLastSlash, allVideoPaths[0].Length - indOfLastSlash);
                int indOfNumber = fileName.LastIndexOf("_");
                numberAndAVI = fileName.Substring(indOfNumber + 1, (fileName.Length - indOfNumber) - 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally { }

            newFileName = currSiteNumber() + "_" + currRevisitDate().Replace("/", "-") + "_" + numberAndAVI;

            return newFileName;
        }

        Image addWhiteSpace(Image src)
        {
            Image newWhiteImg = new Bitmap(800, 800);
            using (Graphics widerGraphics = Graphics.FromImage(newWhiteImg))
            {
                widerGraphics.Clear(Color.White);
                widerGraphics.DrawImageUnscaled(src, 0, 0, src.Width, src.Height);
                return newWhiteImg;
            }


        }

        Image cropAtRect(Image b)
        {
            Image croppedImage = new Bitmap(342, 36);
            using (Graphics croppedGraphics = Graphics.FromImage(croppedImage))
            {
                //cropped image
                croppedGraphics.DrawImage(b, -954, -684);
                //dispose so it doesn't lock the file
                b.Dispose();
                return croppedImage;
            }

        }

        public string currVideoDateAndTime()
        {
           
            string tempName = "temp_vid_still.jpg";
            string tempCroppedName = "temp_still_cropped.jpg";
            string fullTempPath = Path.GetTempPath() + tempName;
            string fullCrTempPath = Path.GetTempPath() + tempCroppedName;

            //save thumbnail
            FFMPEG f = new FFMPEG();
            f.GetThumbnail(allVideoPaths[0], fullTempPath, "1296x720");
            //get all text from image
            var img = cropAtRect(Bitmap.FromFile(fullTempPath));
            img = addWhiteSpace(img);

            img.Save(fullCrTempPath);

            img.Dispose();

            var ocrengine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
            var crpImg = Pix.LoadFromFile(fullCrTempPath);
            var res = ocrengine.Process(crpImg);
            var allText = res.GetText();
            string dateExtr = Regex.Match(allText, "(\\d+)[-.\\/](\\d+)[-.\\/](\\d+)").Value;
            string timeExtr = Regex.Match(allText.ToLower(), "\\b((0?[1-9]|1[012])([:.][0-5][0-9])?(\\s?[ap]m)|([01]?[0-9]|2[0-3])([:.][0-5][0-9]))\\b").Value;
        
            return dateExtr + " " + timeExtr;

        }
        public string getCurrentVideo()
        {
            if (allVideoPaths.Count > 0)
            {
                return allVideoPaths[0];
            }
            else
            {
                return "-1";
            }
        }

        public bool isThisLastVideo()
        {
            if(allVideoPaths.Count == 1)
            {
                return true;
            }
            return false;
        }

        public string[] arrOfVideos()
        {
            return allVideoPaths.ToArray();
        }
        bool moveAndRenameVideo(string origVideo, string newVideo)
        {
            try
            {
                if (!File.Exists(origVideo))
                {
                    MessageBox.Show("Cannot find: " + origVideo + " to move and rename. Have you moved it while this is running?");
                    return false;
                }

                // Ensure that the target does not exist.
                if (File.Exists(newVideo))
                {
                    MessageBox.Show(newVideo + " already exists, cannot move video to entered.");
                    return false;
                }

                
                // Move the file.
                File.Move(origVideo, newVideo);

                if (!File.Exists(newVideo))
                {
                    MessageBox.Show(newVideo + " was not succesfully moved from: " + origVideo + ". Please do this manually.");
                    return false;
                }

                // See if the original exists now.
                if (File.Exists(origVideo))
                {
                    MessageBox.Show(origVideo + " cannot be deleted. Please do this manually");
                }
                else
                {
                    Console.WriteLine("The original file no longer exists, which is expected.");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error moving and renaming video: " + e.Message);
            }

            return true;
        }
        public bool moveCurrentVideo(string newVidName)
        {
            bool moveSucc = moveAndRenameVideo(allVideoPaths[0], newVidName);
            //remove this video 
            allVideoPaths.Remove(allVideoPaths[0]);
            return moveSucc;
        }
    }
}
