﻿namespace CamProto
{
    partial class VideoInputMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoInputMenu));
            this.trackBarVideoTracking = new System.Windows.Forms.TrackBar();
            this.lblCurrDirTxt = new System.Windows.Forms.Label();
            this.btnEnterAndNextVideo = new System.Windows.Forms.Button();
            this.wmpMainPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.txtInitials = new System.Windows.Forms.TextBox();
            this.lblInitials = new System.Windows.Forms.Label();
            this.lblSiteName = new System.Windows.Forms.Label();
            this.txtSiteName = new System.Windows.Forms.TextBox();
            this.lnlSiteID = new System.Windows.Forms.Label();
            this.txtSiteID = new System.Windows.Forms.TextBox();
            this.lblSiteNumber = new System.Windows.Forms.Label();
            this.txtSiteNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTimeAtVideoStart = new System.Windows.Forms.TextBox();
            this.lblvideoDate = new System.Windows.Forms.Label();
            this.txtVideoDate = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblRevisitDate = new System.Windows.Forms.Label();
            this.txtRevisitDate = new System.Windows.Forms.TextBox();
            this.lblVidDuration = new System.Windows.Forms.Label();
            this.txtVideoDuration = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHumanCount = new System.Windows.Forms.Label();
            this.txtHumanCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMaxHumansDrifting = new System.Windows.Forms.TextBox();
            this.lblMaxHumansDrifting = new System.Windows.Forms.Label();
            this.txtMaxDriftBoats = new System.Windows.Forms.TextBox();
            this.lblMaxDriftBoats = new System.Windows.Forms.Label();
            this.txtMaxHumansInNonDriftBoat = new System.Windows.Forms.TextBox();
            this.lblMacHumansInNonDriftBoats = new System.Windows.Forms.Label();
            this.txtMaxNonDriftBoats = new System.Windows.Forms.TextBox();
            this.lblMaxNonDriftboats = new System.Windows.Forms.Label();
            this.txtMaxHumansFishingFromShore = new System.Windows.Forms.TextBox();
            this.lblMaxHumanFishFromShore = new System.Windows.Forms.Label();
            this.txtTotalHumansOnPlatform = new System.Windows.Forms.TextBox();
            this.lblTotalHumansOnPlatform = new System.Windows.Forms.Label();
            this.toolTipLabels = new System.Windows.Forms.ToolTip(this.components);
            this.lblHumanComments = new System.Windows.Forms.Label();
            this.lblOtherAnimal = new System.Windows.Forms.Label();
            this.lblOtherAnimalspeciesName = new System.Windows.Forms.Label();
            this.lblBearCount = new System.Windows.Forms.Label();
            this.lblBearSex1 = new System.Windows.Forms.Label();
            this.lblBearSex2 = new System.Windows.Forms.Label();
            this.lblSex4 = new System.Windows.Forms.Label();
            this.lblSex3 = new System.Windows.Forms.Label();
            this.lblBearAge4 = new System.Windows.Forms.Label();
            this.lblBearAge3 = new System.Windows.Forms.Label();
            this.lblBearAge2 = new System.Windows.Forms.Label();
            this.lblBearAge1 = new System.Windows.Forms.Label();
            this.lblFamilyGroup = new System.Windows.Forms.Label();
            this.lblTotalOtherHumans = new System.Windows.Forms.Label();
            this.lblCOYCount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBearFishInteraction = new System.Windows.Forms.Label();
            this.lblSnagAvoidance = new System.Windows.Forms.Label();
            this.lblSnagAvoidanceComments = new System.Windows.Forms.Label();
            this.lblCamAwarenessComm = new System.Windows.Forms.Label();
            this.lblCamAwareness = new System.Windows.Forms.Label();
            this.lblGeneralComm = new System.Windows.Forms.Label();
            this.lblVerification = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBearSpecies = new System.Windows.Forms.Label();
            this.txtHumanComments = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtOtherAnimSppName = new System.Windows.Forms.TextBox();
            this.txtBearCount = new System.Windows.Forms.TextBox();
            this.txtTotalOtherHumans = new System.Windows.Forms.TextBox();
            this.txtCOYCount = new System.Windows.Forms.TextBox();
            this.txtBoxYearlingCount = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtSnagAvoidanceComments = new System.Windows.Forms.TextBox();
            this.txtCamAwarenessComm = new System.Windows.Forms.TextBox();
            this.txtGeneralComments = new System.Windows.Forms.TextBox();
            this.txtVerification = new System.Windows.Forms.TextBox();
            this.timerVideo = new System.Windows.Forms.Timer(this.components);
            this.txtTargetIssues = new System.Windows.Forms.TextBox();
            this.listBoxCamHandle = new System.Windows.Forms.ComboBox();
            this.listBoxSex1 = new System.Windows.Forms.ComboBox();
            this.listBoxBearAge1 = new System.Windows.Forms.ComboBox();
            this.listBoxFamilyGroup = new System.Windows.Forms.ComboBox();
            this.listBoxBearAge2 = new System.Windows.Forms.ComboBox();
            this.listBoxBearAge3 = new System.Windows.Forms.ComboBox();
            this.listBoxBearAge4 = new System.Windows.Forms.ComboBox();
            this.listBoxSex2 = new System.Windows.Forms.ComboBox();
            this.listBoxSex3 = new System.Windows.Forms.ComboBox();
            this.listBoxSex4 = new System.Windows.Forms.ComboBox();
            this.listBoxBearSpecies = new System.Windows.Forms.ComboBox();
            this.listBoxBearFishInteraction = new System.Windows.Forms.ComboBox();
            this.listBoxSnagAvoidance = new System.Windows.Forms.ComboBox();
            this.listBoxCamAwareness = new System.Windows.Forms.ComboBox();
            this.listBoxOtherAnimal = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVideoTracking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmpMainPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBarVideoTracking
            // 
            this.trackBarVideoTracking.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarVideoTracking.LargeChange = 1;
            this.trackBarVideoTracking.Location = new System.Drawing.Point(5, 530);
            this.trackBarVideoTracking.Maximum = 100;
            this.trackBarVideoTracking.Name = "trackBarVideoTracking";
            this.trackBarVideoTracking.Size = new System.Drawing.Size(863, 45);
            this.trackBarVideoTracking.TabIndex = 100;
            this.trackBarVideoTracking.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarVideoTracking.Scroll += new System.EventHandler(this.trackBarVideoTracking_Scroll);
            // 
            // lblCurrDirTxt
            // 
            this.lblCurrDirTxt.AutoSize = true;
            this.lblCurrDirTxt.Location = new System.Drawing.Point(7, 18);
            this.lblCurrDirTxt.Name = "lblCurrDirTxt";
            this.lblCurrDirTxt.Size = new System.Drawing.Size(74, 13);
            this.lblCurrDirTxt.TabIndex = 2;
            this.lblCurrDirTxt.Text = "Current video:";
            // 
            // btnEnterAndNextVideo
            // 
            this.btnEnterAndNextVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnterAndNextVideo.Location = new System.Drawing.Point(691, 5);
            this.btnEnterAndNextVideo.Name = "btnEnterAndNextVideo";
            this.btnEnterAndNextVideo.Size = new System.Drawing.Size(182, 23);
            this.btnEnterAndNextVideo.TabIndex = 99;
            this.btnEnterAndNextVideo.Text = "Finish and move to next video";
            this.btnEnterAndNextVideo.UseVisualStyleBackColor = true;
            this.btnEnterAndNextVideo.Click += new System.EventHandler(this.btnEnterAndNextVideo_Click);
            // 
            // wmpMainPlayer
            // 
            this.wmpMainPlayer.Enabled = true;
            this.wmpMainPlayer.Location = new System.Drawing.Point(7, 34);
            this.wmpMainPlayer.Name = "wmpMainPlayer";
            this.wmpMainPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmpMainPlayer.OcxState")));
            this.wmpMainPlayer.Size = new System.Drawing.Size(866, 490);
            this.wmpMainPlayer.TabIndex = 0;
            // 
            // txtInitials
            // 
            this.txtInitials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInitials.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtInitials.Location = new System.Drawing.Point(945, 2);
            this.txtInitials.Name = "txtInitials";
            this.txtInitials.Size = new System.Drawing.Size(100, 24);
            this.txtInitials.TabIndex = 1;
            // 
            // lblInitials
            // 
            this.lblInitials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInitials.AutoSize = true;
            this.lblInitials.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblInitials.Location = new System.Drawing.Point(875, 5);
            this.lblInitials.Name = "lblInitials";
            this.lblInitials.Size = new System.Drawing.Size(52, 17);
            this.lblInitials.TabIndex = 6;
            this.lblInitials.Text = "Initials: ";
            this.toolTipLabels.SetToolTip(this.lblInitials, "Your Initials.");
            // 
            // lblSiteName
            // 
            this.lblSiteName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSiteName.AutoSize = true;
            this.lblSiteName.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSiteName.Location = new System.Drawing.Point(874, 33);
            this.lblSiteName.Name = "lblSiteName";
            this.lblSiteName.Size = new System.Drawing.Size(70, 17);
            this.lblSiteName.TabIndex = 8;
            this.lblSiteName.Text = "Site Name:";
            this.toolTipLabels.SetToolTip(this.lblSiteName, "The name of the site, which is indicated in the folder name.");
            // 
            // txtSiteName
            // 
            this.txtSiteName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSiteName.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtSiteName.Location = new System.Drawing.Point(945, 30);
            this.txtSiteName.Name = "txtSiteName";
            this.txtSiteName.Size = new System.Drawing.Size(100, 24);
            this.txtSiteName.TabIndex = 3;
            // 
            // lnlSiteID
            // 
            this.lnlSiteID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnlSiteID.AutoSize = true;
            this.lnlSiteID.Font = new System.Drawing.Font("Calibri", 10F);
            this.lnlSiteID.Location = new System.Drawing.Point(876, 61);
            this.lnlSiteID.Name = "lnlSiteID";
            this.lnlSiteID.Size = new System.Drawing.Size(49, 17);
            this.lnlSiteID.TabIndex = 12;
            this.lnlSiteID.Text = "Site ID:";
            this.toolTipLabels.SetToolTip(this.lnlSiteID, "The ID is composed of the year and the site number, e.g. 2019AT18.");
            // 
            // txtSiteID
            // 
            this.txtSiteID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSiteID.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtSiteID.Location = new System.Drawing.Point(945, 58);
            this.txtSiteID.Name = "txtSiteID";
            this.txtSiteID.Size = new System.Drawing.Size(100, 24);
            this.txtSiteID.TabIndex = 5;
            // 
            // lblSiteNumber
            // 
            this.lblSiteNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSiteNumber.AutoSize = true;
            this.lblSiteNumber.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSiteNumber.Location = new System.Drawing.Point(1054, 6);
            this.lblSiteNumber.Name = "lblSiteNumber";
            this.lblSiteNumber.Size = new System.Drawing.Size(82, 17);
            this.lblSiteNumber.TabIndex = 10;
            this.lblSiteNumber.Text = "Site Number:";
            this.toolTipLabels.SetToolTip(this.lblSiteNumber, "The number of the site, which is indicated in the folder name. It appears before " +
        "the site name. ");
            // 
            // txtSiteNumber
            // 
            this.txtSiteNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSiteNumber.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtSiteNumber.Location = new System.Drawing.Point(1139, 3);
            this.txtSiteNumber.Name = "txtSiteNumber";
            this.txtSiteNumber.Size = new System.Drawing.Size(100, 24);
            this.txtSiteNumber.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 10F);
            this.label5.Location = new System.Drawing.Point(874, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Time (24hr) of Video Start:";
            this.toolTipLabels.SetToolTip(this.label5, "Time the video starts or image captured (24hr).");
            // 
            // txtTimeAtVideoStart
            // 
            this.txtTimeAtVideoStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTimeAtVideoStart.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtTimeAtVideoStart.Location = new System.Drawing.Point(1046, 172);
            this.txtTimeAtVideoStart.Name = "txtTimeAtVideoStart";
            this.txtTimeAtVideoStart.Size = new System.Drawing.Size(100, 24);
            this.txtTimeAtVideoStart.TabIndex = 9;
            // 
            // lblvideoDate
            // 
            this.lblvideoDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblvideoDate.AutoSize = true;
            this.lblvideoDate.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblvideoDate.Location = new System.Drawing.Point(874, 116);
            this.lblvideoDate.Name = "lblvideoDate";
            this.lblvideoDate.Size = new System.Drawing.Size(166, 17);
            this.lblvideoDate.TabIndex = 18;
            this.lblvideoDate.Text = "Video Date (DD/MM/YYYY):";
            this.toolTipLabels.SetToolTip(this.lblvideoDate, "Date of image (dd/mm/yyyy).");
            // 
            // txtVideoDate
            // 
            this.txtVideoDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideoDate.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtVideoDate.Location = new System.Drawing.Point(1046, 113);
            this.txtVideoDate.Name = "txtVideoDate";
            this.txtVideoDate.Size = new System.Drawing.Size(100, 24);
            this.txtVideoDate.TabIndex = 7;
            // 
            // lblFileName
            // 
            this.lblFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblFileName.Location = new System.Drawing.Point(874, 89);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(68, 17);
            this.lblFileName.TabIndex = 16;
            this.lblFileName.Text = "File Name:";
            this.toolTipLabels.SetToolTip(this.lblFileName, resources.GetString("lblFileName.ToolTip"));
            // 
            // txtFileName
            // 
            this.txtFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileName.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtFileName.Location = new System.Drawing.Point(945, 86);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(201, 24);
            this.txtFileName.TabIndex = 6;
            // 
            // lblRevisitDate
            // 
            this.lblRevisitDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRevisitDate.AutoSize = true;
            this.lblRevisitDate.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblRevisitDate.Location = new System.Drawing.Point(1053, 36);
            this.lblRevisitDate.Name = "lblRevisitDate";
            this.lblRevisitDate.Size = new System.Drawing.Size(80, 17);
            this.lblRevisitDate.TabIndex = 14;
            this.lblRevisitDate.Text = "Revisit Date:";
            this.toolTipLabels.SetToolTip(this.lblRevisitDate, "Date of revisit (dd/mm/yyyy). (i.e., the revisit date in the revisit date file na" +
        "me).");
            // 
            // txtRevisitDate
            // 
            this.txtRevisitDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRevisitDate.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtRevisitDate.Location = new System.Drawing.Point(1139, 33);
            this.txtRevisitDate.Name = "txtRevisitDate";
            this.txtRevisitDate.Size = new System.Drawing.Size(100, 24);
            this.txtRevisitDate.TabIndex = 4;
            // 
            // lblVidDuration
            // 
            this.lblVidDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVidDuration.AutoSize = true;
            this.lblVidDuration.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblVidDuration.Location = new System.Drawing.Point(874, 146);
            this.lblVidDuration.Name = "lblVidDuration";
            this.lblVidDuration.Size = new System.Drawing.Size(170, 17);
            this.lblVidDuration.TabIndex = 22;
            this.lblVidDuration.Text = "Video Duration (HH:MM:SS):";
            this.toolTipLabels.SetToolTip(this.lblVidDuration, "Duration of the video (hh:mm:ss). Indicate N/A if it is a still image.");
            // 
            // txtVideoDuration
            // 
            this.txtVideoDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideoDuration.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtVideoDuration.Location = new System.Drawing.Point(1046, 143);
            this.txtVideoDuration.Name = "txtVideoDuration";
            this.txtVideoDuration.Size = new System.Drawing.Size(100, 24);
            this.txtVideoDuration.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(878, 200);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(379, 10);
            this.panel1.TabIndex = 23;
            // 
            // lblHumanCount
            // 
            this.lblHumanCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHumanCount.AutoSize = true;
            this.lblHumanCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblHumanCount.Location = new System.Drawing.Point(876, 216);
            this.lblHumanCount.Name = "lblHumanCount";
            this.lblHumanCount.Size = new System.Drawing.Size(89, 17);
            this.lblHumanCount.TabIndex = 24;
            this.lblHumanCount.Text = "Human Count:";
            this.toolTipLabels.SetToolTip(this.lblHumanCount, "Max humans detected (including land and boat). This is the max human\'s throughout" +
        " the duration of the video. RESEARCHERS ARE NOT INCLUDED IN THE HUMAN COUNT (i.e" +
        "., people setting up the cameras). ");
            // 
            // txtHumanCount
            // 
            this.txtHumanCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHumanCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtHumanCount.Location = new System.Drawing.Point(1069, 212);
            this.txtHumanCount.MaxLength = 2;
            this.txtHumanCount.Name = "txtHumanCount";
            this.txtHumanCount.Size = new System.Drawing.Size(100, 24);
            this.txtHumanCount.TabIndex = 10;
            this.txtHumanCount.Text = "0";
            this.txtHumanCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHumanCount_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10F);
            this.label1.Location = new System.Drawing.Point(876, 246);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "Researcher Cam Handle:";
            this.toolTipLabels.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // txtMaxHumansDrifting
            // 
            this.txtMaxHumansDrifting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxHumansDrifting.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtMaxHumansDrifting.Location = new System.Drawing.Point(1069, 269);
            this.txtMaxHumansDrifting.MaxLength = 12;
            this.txtMaxHumansDrifting.Name = "txtMaxHumansDrifting";
            this.txtMaxHumansDrifting.Size = new System.Drawing.Size(100, 24);
            this.txtMaxHumansDrifting.TabIndex = 29;
            this.txtMaxHumansDrifting.Text = "0";
            this.txtMaxHumansDrifting.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxHumansDrifting_TextChanged);
            // 
            // lblMaxHumansDrifting
            // 
            this.lblMaxHumansDrifting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaxHumansDrifting.AutoSize = true;
            this.lblMaxHumansDrifting.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblMaxHumansDrifting.Location = new System.Drawing.Point(876, 273);
            this.lblMaxHumansDrifting.Name = "lblMaxHumansDrifting";
            this.lblMaxHumansDrifting.Size = new System.Drawing.Size(132, 17);
            this.lblMaxHumansDrifting.TabIndex = 28;
            this.lblMaxHumansDrifting.Text = "Max Humans Drifting:";
            this.toolTipLabels.SetToolTip(this.lblMaxHumansDrifting, "Max humans in rafts (these are the inflatable boats).");
            // 
            // txtMaxDriftBoats
            // 
            this.txtMaxDriftBoats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxDriftBoats.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtMaxDriftBoats.Location = new System.Drawing.Point(1069, 299);
            this.txtMaxDriftBoats.MaxLength = 13;
            this.txtMaxDriftBoats.Name = "txtMaxDriftBoats";
            this.txtMaxDriftBoats.Size = new System.Drawing.Size(100, 24);
            this.txtMaxDriftBoats.TabIndex = 31;
            this.txtMaxDriftBoats.Text = "0";
            this.txtMaxDriftBoats.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxDriftBoats_TextChanged);
            // 
            // lblMaxDriftBoats
            // 
            this.lblMaxDriftBoats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaxDriftBoats.AutoSize = true;
            this.lblMaxDriftBoats.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblMaxDriftBoats.Location = new System.Drawing.Point(876, 303);
            this.lblMaxDriftBoats.Name = "lblMaxDriftBoats";
            this.lblMaxDriftBoats.Size = new System.Drawing.Size(97, 17);
            this.lblMaxDriftBoats.TabIndex = 30;
            this.lblMaxDriftBoats.Text = "Max Driftboats:";
            this.toolTipLabels.SetToolTip(this.lblMaxDriftBoats, "Max number of drift boats themselves (probably never more than 3)");
            // 
            // txtMaxHumansInNonDriftBoat
            // 
            this.txtMaxHumansInNonDriftBoat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxHumansInNonDriftBoat.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtMaxHumansInNonDriftBoat.Location = new System.Drawing.Point(1069, 329);
            this.txtMaxHumansInNonDriftBoat.MaxLength = 14;
            this.txtMaxHumansInNonDriftBoat.Name = "txtMaxHumansInNonDriftBoat";
            this.txtMaxHumansInNonDriftBoat.Size = new System.Drawing.Size(100, 24);
            this.txtMaxHumansInNonDriftBoat.TabIndex = 33;
            this.txtMaxHumansInNonDriftBoat.Text = "0";
            this.txtMaxHumansInNonDriftBoat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxHumansInNonDriftBoat_TextChanged);
            // 
            // lblMacHumansInNonDriftBoats
            // 
            this.lblMacHumansInNonDriftBoats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMacHumansInNonDriftBoats.AutoSize = true;
            this.lblMacHumansInNonDriftBoats.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblMacHumansInNonDriftBoats.Location = new System.Drawing.Point(876, 333);
            this.lblMacHumansInNonDriftBoats.Name = "lblMacHumansInNonDriftBoats";
            this.lblMacHumansInNonDriftBoats.Size = new System.Drawing.Size(184, 17);
            this.lblMacHumansInNonDriftBoats.TabIndex = 32;
            this.lblMacHumansInNonDriftBoats.Text = "Max Humans in Non Drift Boat:";
            this.toolTipLabels.SetToolTip(this.lblMacHumansInNonDriftBoats, "Max humans in boat other than inflatable rafts. ");
            // 
            // txtMaxNonDriftBoats
            // 
            this.txtMaxNonDriftBoats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxNonDriftBoats.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtMaxNonDriftBoats.Location = new System.Drawing.Point(1069, 359);
            this.txtMaxNonDriftBoats.MaxLength = 15;
            this.txtMaxNonDriftBoats.Name = "txtMaxNonDriftBoats";
            this.txtMaxNonDriftBoats.Size = new System.Drawing.Size(100, 24);
            this.txtMaxNonDriftBoats.TabIndex = 35;
            this.txtMaxNonDriftBoats.Text = "0";
            this.txtMaxNonDriftBoats.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxNonDriftBoats_TextChanged);
            // 
            // lblMaxNonDriftboats
            // 
            this.lblMaxNonDriftboats.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaxNonDriftboats.AutoSize = true;
            this.lblMaxNonDriftboats.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblMaxNonDriftboats.Location = new System.Drawing.Point(876, 363);
            this.lblMaxNonDriftboats.Name = "lblMaxNonDriftboats";
            this.lblMaxNonDriftboats.Size = new System.Drawing.Size(124, 17);
            this.lblMaxNonDriftboats.TabIndex = 34;
            this.lblMaxNonDriftboats.Text = "Max Non-Driftboats:";
            this.toolTipLabels.SetToolTip(this.lblMaxNonDriftboats, "Max number of non-driftboats (e.g., aluminum boat, kayak)");
            // 
            // txtMaxHumansFishingFromShore
            // 
            this.txtMaxHumansFishingFromShore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaxHumansFishingFromShore.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtMaxHumansFishingFromShore.Location = new System.Drawing.Point(1069, 389);
            this.txtMaxHumansFishingFromShore.MaxLength = 16;
            this.txtMaxHumansFishingFromShore.Name = "txtMaxHumansFishingFromShore";
            this.txtMaxHumansFishingFromShore.Size = new System.Drawing.Size(100, 24);
            this.txtMaxHumansFishingFromShore.TabIndex = 37;
            this.txtMaxHumansFishingFromShore.Text = "0";
            this.txtMaxHumansFishingFromShore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxHumansFishingFromShore_TextChanged);
            // 
            // lblMaxHumanFishFromShore
            // 
            this.lblMaxHumanFishFromShore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMaxHumanFishFromShore.AutoSize = true;
            this.lblMaxHumanFishFromShore.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblMaxHumanFishFromShore.Location = new System.Drawing.Point(876, 393);
            this.lblMaxHumanFishFromShore.Name = "lblMaxHumanFishFromShore";
            this.lblMaxHumanFishFromShore.Size = new System.Drawing.Size(194, 17);
            this.lblMaxHumanFishFromShore.TabIndex = 36;
            this.lblMaxHumanFishFromShore.Text = "Max Humans Fishing From Shore:";
            this.toolTipLabels.SetToolTip(this.lblMaxHumanFishFromShore, "Max humans fishing from shore");
            // 
            // txtTotalHumansOnPlatform
            // 
            this.txtTotalHumansOnPlatform.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalHumansOnPlatform.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtTotalHumansOnPlatform.Location = new System.Drawing.Point(1069, 419);
            this.txtTotalHumansOnPlatform.MaxLength = 17;
            this.txtTotalHumansOnPlatform.Name = "txtTotalHumansOnPlatform";
            this.txtTotalHumansOnPlatform.Size = new System.Drawing.Size(100, 24);
            this.txtTotalHumansOnPlatform.TabIndex = 39;
            this.txtTotalHumansOnPlatform.Text = "0";
            this.txtTotalHumansOnPlatform.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalHumansOnPlatform_TextChanged);
            // 
            // lblTotalHumansOnPlatform
            // 
            this.lblTotalHumansOnPlatform.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalHumansOnPlatform.AutoSize = true;
            this.lblTotalHumansOnPlatform.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblTotalHumansOnPlatform.Location = new System.Drawing.Point(876, 423);
            this.lblTotalHumansOnPlatform.Name = "lblTotalHumansOnPlatform";
            this.lblTotalHumansOnPlatform.Size = new System.Drawing.Size(158, 17);
            this.lblTotalHumansOnPlatform.TabIndex = 38;
            this.lblTotalHumansOnPlatform.Text = "Total Humans on Platform:";
            this.toolTipLabels.SetToolTip(this.lblTotalHumansOnPlatform, "Maximum humans on wildlife viewing platform. This will only be relevant for the f" +
        "ollowing folders: AT24 Belarko South Side, AT25 Little Tweedsmuir, AT26 Big Twee" +
        "dsmuir, AT27 Fisheries Pool");
            // 
            // lblHumanComments
            // 
            this.lblHumanComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHumanComments.AutoSize = true;
            this.lblHumanComments.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblHumanComments.Location = new System.Drawing.Point(876, 483);
            this.lblHumanComments.Name = "lblHumanComments";
            this.lblHumanComments.Size = new System.Drawing.Size(116, 17);
            this.lblHumanComments.TabIndex = 42;
            this.lblHumanComments.Text = "Human Comments:";
            this.toolTipLabels.SetToolTip(this.lblHumanComments, "Comments regarding human activity, if any.");
            // 
            // lblOtherAnimal
            // 
            this.lblOtherAnimal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOtherAnimal.AutoSize = true;
            this.lblOtherAnimal.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblOtherAnimal.Location = new System.Drawing.Point(874, 518);
            this.lblOtherAnimal.Name = "lblOtherAnimal";
            this.lblOtherAnimal.Size = new System.Drawing.Size(133, 17);
            this.lblOtherAnimal.TabIndex = 44;
            this.lblOtherAnimal.Text = "Other Animal Present:";
            this.toolTipLabels.SetToolTip(this.lblOtherAnimal, "Indicate whether animals other than bears were detected.");
            // 
            // lblOtherAnimalspeciesName
            // 
            this.lblOtherAnimalspeciesName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOtherAnimalspeciesName.AutoSize = true;
            this.lblOtherAnimalspeciesName.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblOtherAnimalspeciesName.Location = new System.Drawing.Point(874, 544);
            this.lblOtherAnimalspeciesName.Name = "lblOtherAnimalspeciesName";
            this.lblOtherAnimalspeciesName.Size = new System.Drawing.Size(221, 17);
            this.lblOtherAnimalspeciesName.TabIndex = 46;
            this.lblOtherAnimalspeciesName.Text = "Other Animal Common Species Name:";
            this.toolTipLabels.SetToolTip(this.lblOtherAnimalspeciesName, "If known, indicate the species (common name fine).");
            // 
            // lblBearCount
            // 
            this.lblBearCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearCount.AutoSize = true;
            this.lblBearCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearCount.Location = new System.Drawing.Point(12, 577);
            this.lblBearCount.Name = "lblBearCount";
            this.lblBearCount.Size = new System.Drawing.Size(75, 17);
            this.lblBearCount.TabIndex = 48;
            this.lblBearCount.Text = "Bear Count:";
            this.toolTipLabels.SetToolTip(this.lblBearCount, "Indicate the total number of bears detected in the video or still image.");
            // 
            // lblBearSex1
            // 
            this.lblBearSex1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearSex1.AutoSize = true;
            this.lblBearSex1.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearSex1.Location = new System.Drawing.Point(202, 577);
            this.lblBearSex1.Name = "lblBearSex1";
            this.lblBearSex1.Size = new System.Drawing.Size(41, 17);
            this.lblBearSex1.TabIndex = 52;
            this.lblBearSex1.Text = "Sex 1:";
            this.toolTipLabels.SetToolTip(this.lblBearSex1, resources.GetString("lblBearSex1.ToolTip"));
            // 
            // lblBearSex2
            // 
            this.lblBearSex2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearSex2.AutoSize = true;
            this.lblBearSex2.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearSex2.Location = new System.Drawing.Point(202, 604);
            this.lblBearSex2.Name = "lblBearSex2";
            this.lblBearSex2.Size = new System.Drawing.Size(41, 17);
            this.lblBearSex2.TabIndex = 54;
            this.lblBearSex2.Text = "Sex 2:";
            this.toolTipLabels.SetToolTip(this.lblBearSex2, "If there is more than one adult/subadult bear, indicate the sex here, if known. I" +
        "ndicate N/A if no bear detected ");
            // 
            // lblSex4
            // 
            this.lblSex4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSex4.AutoSize = true;
            this.lblSex4.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSex4.Location = new System.Drawing.Point(202, 660);
            this.lblSex4.Name = "lblSex4";
            this.lblSex4.Size = new System.Drawing.Size(41, 17);
            this.lblSex4.TabIndex = 58;
            this.lblSex4.Text = "Sex 4:";
            this.toolTipLabels.SetToolTip(this.lblSex4, "If there is more than one adult/subadult bear, indicate the sex here, if known. I" +
        "ndicate N/A if no bear detected ");
            // 
            // lblSex3
            // 
            this.lblSex3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSex3.AutoSize = true;
            this.lblSex3.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSex3.Location = new System.Drawing.Point(202, 633);
            this.lblSex3.Name = "lblSex3";
            this.lblSex3.Size = new System.Drawing.Size(41, 17);
            this.lblSex3.TabIndex = 56;
            this.lblSex3.Text = "Sex 3:";
            this.toolTipLabels.SetToolTip(this.lblSex3, "If there is more than one adult/subadult bear, indicate the sex here, if known. I" +
        "ndicate N/A if no bear detected ");
            // 
            // lblBearAge4
            // 
            this.lblBearAge4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearAge4.AutoSize = true;
            this.lblBearAge4.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearAge4.Location = new System.Drawing.Point(350, 660);
            this.lblBearAge4.Name = "lblBearAge4";
            this.lblBearAge4.Size = new System.Drawing.Size(44, 17);
            this.lblBearAge4.TabIndex = 66;
            this.lblBearAge4.Text = "Age 4:";
            this.toolTipLabels.SetToolTip(this.lblBearAge4, "Indicate the age class of the adult or subadult bear under column \'sex4\'. ");
            // 
            // lblBearAge3
            // 
            this.lblBearAge3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearAge3.AutoSize = true;
            this.lblBearAge3.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearAge3.Location = new System.Drawing.Point(350, 633);
            this.lblBearAge3.Name = "lblBearAge3";
            this.lblBearAge3.Size = new System.Drawing.Size(44, 17);
            this.lblBearAge3.TabIndex = 64;
            this.lblBearAge3.Text = "Age 3:";
            this.toolTipLabels.SetToolTip(this.lblBearAge3, "Indicate the age class of the adult or subadult bear under column \'sex3\'. ");
            // 
            // lblBearAge2
            // 
            this.lblBearAge2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearAge2.AutoSize = true;
            this.lblBearAge2.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearAge2.Location = new System.Drawing.Point(350, 604);
            this.lblBearAge2.Name = "lblBearAge2";
            this.lblBearAge2.Size = new System.Drawing.Size(44, 17);
            this.lblBearAge2.TabIndex = 62;
            this.lblBearAge2.Text = "Age 2:";
            this.toolTipLabels.SetToolTip(this.lblBearAge2, "Indicate the age class of the adult or subadult bear under column \'sex2\'. ");
            // 
            // lblBearAge1
            // 
            this.lblBearAge1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearAge1.AutoSize = true;
            this.lblBearAge1.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearAge1.Location = new System.Drawing.Point(350, 577);
            this.lblBearAge1.Name = "lblBearAge1";
            this.lblBearAge1.Size = new System.Drawing.Size(44, 17);
            this.lblBearAge1.TabIndex = 60;
            this.lblBearAge1.Text = "Age 1:";
            this.toolTipLabels.SetToolTip(this.lblBearAge1, "Indicate the age class of the adult or subadult bear under column \'sex1\'. ");
            // 
            // lblFamilyGroup
            // 
            this.lblFamilyGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFamilyGroup.AutoSize = true;
            this.lblFamilyGroup.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblFamilyGroup.Location = new System.Drawing.Point(497, 577);
            this.lblFamilyGroup.Name = "lblFamilyGroup";
            this.lblFamilyGroup.Size = new System.Drawing.Size(86, 17);
            this.lblFamilyGroup.TabIndex = 68;
            this.lblFamilyGroup.Text = "Family Group:";
            this.toolTipLabels.SetToolTip(this.lblFamilyGroup, "Indicate whether there was a family group (i.e., a sow with yearlings or cubs of " +
        "the year). Indicate N/A if no bear detected.");
            // 
            // lblTotalOtherHumans
            // 
            this.lblTotalOtherHumans.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalOtherHumans.AutoSize = true;
            this.lblTotalOtherHumans.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblTotalOtherHumans.Location = new System.Drawing.Point(876, 453);
            this.lblTotalOtherHumans.Name = "lblTotalOtherHumans";
            this.lblTotalOtherHumans.Size = new System.Drawing.Size(125, 17);
            this.lblTotalOtherHumans.TabIndex = 40;
            this.lblTotalOtherHumans.Text = "Total Other Humans:";
            this.toolTipLabels.SetToolTip(this.lblTotalOtherHumans, "Number of other humans that are engaged in an activity other than the ones mentio" +
        "ned above.");
            // 
            // lblCOYCount
            // 
            this.lblCOYCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCOYCount.AutoSize = true;
            this.lblCOYCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblCOYCount.Location = new System.Drawing.Point(497, 604);
            this.lblCOYCount.Name = "lblCOYCount";
            this.lblCOYCount.Size = new System.Drawing.Size(133, 17);
            this.lblCOYCount.TabIndex = 70;
            this.lblCOYCount.Text = "Cub of the Year Count:";
            this.toolTipLabels.SetToolTip(this.lblCOYCount, "Indicate the number of cubs of the year. Indicate N/A if no bear detected.");
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10F);
            this.label2.Location = new System.Drawing.Point(497, 633);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 72;
            this.label2.Text = "Yearling Count:";
            this.toolTipLabels.SetToolTip(this.label2, "Indicate the number of yearlings. Indicate N/A if no bear detected.");
            // 
            // lblBearFishInteraction
            // 
            this.lblBearFishInteraction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearFishInteraction.AutoSize = true;
            this.lblBearFishInteraction.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearFishInteraction.Location = new System.Drawing.Point(752, 581);
            this.lblBearFishInteraction.Name = "lblBearFishInteraction";
            this.lblBearFishInteraction.Size = new System.Drawing.Size(130, 17);
            this.lblBearFishInteraction.TabIndex = 74;
            this.lblBearFishInteraction.Text = "Bear-Fish Interaction:";
            this.toolTipLabels.SetToolTip(this.lblBearFishInteraction, resources.GetString("lblBearFishInteraction.ToolTip"));
            // 
            // lblSnagAvoidance
            // 
            this.lblSnagAvoidance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSnagAvoidance.AutoSize = true;
            this.lblSnagAvoidance.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSnagAvoidance.Location = new System.Drawing.Point(752, 607);
            this.lblSnagAvoidance.Name = "lblSnagAvoidance";
            this.lblSnagAvoidance.Size = new System.Drawing.Size(100, 17);
            this.lblSnagAvoidance.TabIndex = 76;
            this.lblSnagAvoidance.Text = "Snag Avoidance:";
            this.toolTipLabels.SetToolTip(this.lblSnagAvoidance, "Indicate whether the bear avoids interacting with a hair snag. Indicate N/A if no" +
        " bear detected.");
            // 
            // lblSnagAvoidanceComments
            // 
            this.lblSnagAvoidanceComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSnagAvoidanceComments.AutoSize = true;
            this.lblSnagAvoidanceComments.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblSnagAvoidanceComments.Location = new System.Drawing.Point(752, 636);
            this.lblSnagAvoidanceComments.Name = "lblSnagAvoidanceComments";
            this.lblSnagAvoidanceComments.Size = new System.Drawing.Size(163, 17);
            this.lblSnagAvoidanceComments.TabIndex = 78;
            this.lblSnagAvoidanceComments.Text = "Snag Avoidance Comments:";
            this.toolTipLabels.SetToolTip(this.lblSnagAvoidanceComments, "If yes to snag avoidance, describe behaviour.");
            // 
            // lblCamAwarenessComm
            // 
            this.lblCamAwarenessComm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamAwarenessComm.AutoSize = true;
            this.lblCamAwarenessComm.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblCamAwarenessComm.Location = new System.Drawing.Point(752, 686);
            this.lblCamAwarenessComm.Name = "lblCamAwarenessComm";
            this.lblCamAwarenessComm.Size = new System.Drawing.Size(164, 17);
            this.lblCamAwarenessComm.TabIndex = 82;
            this.lblCamAwarenessComm.Text = "Cam Awareness Comments:";
            this.toolTipLabels.SetToolTip(this.lblCamAwarenessComm, "If yes to cam awareness, describe behaviour.");
            // 
            // lblCamAwareness
            // 
            this.lblCamAwareness.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCamAwareness.AutoSize = true;
            this.lblCamAwareness.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblCamAwareness.Location = new System.Drawing.Point(752, 660);
            this.lblCamAwareness.Name = "lblCamAwareness";
            this.lblCamAwareness.Size = new System.Drawing.Size(101, 17);
            this.lblCamAwareness.TabIndex = 80;
            this.lblCamAwareness.Text = "Cam Awareness:";
            this.toolTipLabels.SetToolTip(this.lblCamAwareness, "Indicate whether the bear behaviourally demonstrates awareness of the camera (e.g" +
        "., alters path or changes orientation upon noticing camera). Indicate N/A if no " +
        "bear detected.");
            // 
            // lblGeneralComm
            // 
            this.lblGeneralComm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGeneralComm.AutoSize = true;
            this.lblGeneralComm.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblGeneralComm.Location = new System.Drawing.Point(1029, 585);
            this.lblGeneralComm.Name = "lblGeneralComm";
            this.lblGeneralComm.Size = new System.Drawing.Size(120, 17);
            this.lblGeneralComm.TabIndex = 84;
            this.lblGeneralComm.Text = "General Comments:";
            this.toolTipLabels.SetToolTip(this.lblGeneralComm, "Any comments that could be useful (e.g., flagging potential date error).");
            // 
            // lblVerification
            // 
            this.lblVerification.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVerification.AutoSize = true;
            this.lblVerification.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblVerification.Location = new System.Drawing.Point(1030, 618);
            this.lblVerification.Name = "lblVerification";
            this.lblVerification.Size = new System.Drawing.Size(130, 17);
            this.lblVerification.TabIndex = 86;
            this.lblVerification.Text = "Verification Required:";
            this.toolTipLabels.SetToolTip(this.lblVerification, resources.GetString("lblVerification.ToolTip"));
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10F);
            this.label3.Location = new System.Drawing.Point(1030, 648);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 88;
            this.label3.Text = "Target Issues:";
            this.toolTipLabels.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblBearSpecies
            // 
            this.lblBearSpecies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBearSpecies.AutoSize = true;
            this.lblBearSpecies.Font = new System.Drawing.Font("Calibri", 10F);
            this.lblBearSpecies.Location = new System.Drawing.Point(12, 607);
            this.lblBearSpecies.Name = "lblBearSpecies";
            this.lblBearSpecies.Size = new System.Drawing.Size(83, 17);
            this.lblBearSpecies.TabIndex = 50;
            this.lblBearSpecies.Text = "Bear Species:";
            // 
            // txtHumanComments
            // 
            this.txtHumanComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHumanComments.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtHumanComments.Location = new System.Drawing.Point(1069, 479);
            this.txtHumanComments.MaxLength = 100;
            this.txtHumanComments.Name = "txtHumanComments";
            this.txtHumanComments.Size = new System.Drawing.Size(100, 24);
            this.txtHumanComments.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Location = new System.Drawing.Point(879, 504);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(379, 10);
            this.panel2.TabIndex = 24;
            // 
            // txtOtherAnimSppName
            // 
            this.txtOtherAnimSppName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOtherAnimSppName.Enabled = false;
            this.txtOtherAnimSppName.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtOtherAnimSppName.Location = new System.Drawing.Point(1102, 541);
            this.txtOtherAnimSppName.MaxLength = 30;
            this.txtOtherAnimSppName.Name = "txtOtherAnimSppName";
            this.txtOtherAnimSppName.Size = new System.Drawing.Size(100, 24);
            this.txtOtherAnimSppName.TabIndex = 21;
            // 
            // txtBearCount
            // 
            this.txtBearCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBearCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtBearCount.Location = new System.Drawing.Point(93, 574);
            this.txtBearCount.MaxLength = 2;
            this.txtBearCount.Name = "txtBearCount";
            this.txtBearCount.Size = new System.Drawing.Size(100, 24);
            this.txtBearCount.TabIndex = 22;
            this.txtBearCount.Text = "0";
            this.txtBearCount.TextChanged += new System.EventHandler(this.txtBearCount_TextChanged);
            this.txtBearCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBearCount_TextChanged);
            this.txtBearCount.Leave += new System.EventHandler(this.txtBearCount_LostFocus);
            // 
            // txtTotalOtherHumans
            // 
            this.txtTotalOtherHumans.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalOtherHumans.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtTotalOtherHumans.Location = new System.Drawing.Point(1069, 449);
            this.txtTotalOtherHumans.MaxLength = 18;
            this.txtTotalOtherHumans.Name = "txtTotalOtherHumans";
            this.txtTotalOtherHumans.Size = new System.Drawing.Size(100, 24);
            this.txtTotalOtherHumans.TabIndex = 41;
            this.txtTotalOtherHumans.Text = "0";
            this.txtTotalOtherHumans.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalOtherHumans_TextChanged);
            // 
            // txtCOYCount
            // 
            this.txtCOYCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCOYCount.Enabled = false;
            this.txtCOYCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtCOYCount.Location = new System.Drawing.Point(637, 600);
            this.txtCOYCount.MaxLength = 3;
            this.txtCOYCount.Name = "txtCOYCount";
            this.txtCOYCount.Size = new System.Drawing.Size(100, 24);
            this.txtCOYCount.TabIndex = 33;
            this.txtCOYCount.Text = "N/A";
            this.txtCOYCount.Leave += new System.EventHandler(this.txtCOYCount_LostFocus);
            // 
            // txtBoxYearlingCount
            // 
            this.txtBoxYearlingCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBoxYearlingCount.Enabled = false;
            this.txtBoxYearlingCount.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtBoxYearlingCount.Location = new System.Drawing.Point(637, 629);
            this.txtBoxYearlingCount.MaxLength = 3;
            this.txtBoxYearlingCount.Name = "txtBoxYearlingCount";
            this.txtBoxYearlingCount.Size = new System.Drawing.Size(100, 24);
            this.txtBoxYearlingCount.TabIndex = 34;
            this.txtBoxYearlingCount.Text = "N/A";
            this.txtBoxYearlingCount.Leave += new System.EventHandler(this.txtCOYCount_LostFocus);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Location = new System.Drawing.Point(875, 567);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(379, 10);
            this.panel3.TabIndex = 25;
            // 
            // txtSnagAvoidanceComments
            // 
            this.txtSnagAvoidanceComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSnagAvoidanceComments.Enabled = false;
            this.txtSnagAvoidanceComments.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtSnagAvoidanceComments.Location = new System.Drawing.Point(915, 632);
            this.txtSnagAvoidanceComments.MaxLength = 100;
            this.txtSnagAvoidanceComments.Name = "txtSnagAvoidanceComments";
            this.txtSnagAvoidanceComments.Size = new System.Drawing.Size(100, 24);
            this.txtSnagAvoidanceComments.TabIndex = 37;
            // 
            // txtCamAwarenessComm
            // 
            this.txtCamAwarenessComm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCamAwarenessComm.Enabled = false;
            this.txtCamAwarenessComm.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtCamAwarenessComm.Location = new System.Drawing.Point(915, 682);
            this.txtCamAwarenessComm.MaxLength = 100;
            this.txtCamAwarenessComm.Name = "txtCamAwarenessComm";
            this.txtCamAwarenessComm.Size = new System.Drawing.Size(100, 24);
            this.txtCamAwarenessComm.TabIndex = 39;
            // 
            // txtGeneralComments
            // 
            this.txtGeneralComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGeneralComments.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtGeneralComments.Location = new System.Drawing.Point(1155, 582);
            this.txtGeneralComments.MaxLength = 100;
            this.txtGeneralComments.Name = "txtGeneralComments";
            this.txtGeneralComments.Size = new System.Drawing.Size(100, 24);
            this.txtGeneralComments.TabIndex = 40;
            // 
            // txtVerification
            // 
            this.txtVerification.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVerification.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtVerification.Location = new System.Drawing.Point(1156, 615);
            this.txtVerification.MaxLength = 100;
            this.txtVerification.Name = "txtVerification";
            this.txtVerification.Size = new System.Drawing.Size(100, 24);
            this.txtVerification.TabIndex = 41;
            // 
            // txtTargetIssues
            // 
            this.txtTargetIssues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTargetIssues.Font = new System.Drawing.Font("Calibri", 10F);
            this.txtTargetIssues.Location = new System.Drawing.Point(1156, 645);
            this.txtTargetIssues.MaxLength = 100;
            this.txtTargetIssues.Name = "txtTargetIssues";
            this.txtTargetIssues.Size = new System.Drawing.Size(100, 24);
            this.txtTargetIssues.TabIndex = 42;
            // 
            // listBoxCamHandle
            // 
            this.listBoxCamHandle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxCamHandle.FormattingEnabled = true;
            this.listBoxCamHandle.Items.AddRange(new object[] {
            "No Researcher Cam Handle",
            "Start of Video",
            "Middle of Video",
            "End of Video"});
            this.listBoxCamHandle.Location = new System.Drawing.Point(1069, 242);
            this.listBoxCamHandle.Name = "listBoxCamHandle";
            this.listBoxCamHandle.Size = new System.Drawing.Size(133, 21);
            this.listBoxCamHandle.TabIndex = 11;
            this.listBoxCamHandle.SelectedIndexChanged += new System.EventHandler(this.listBoxCamHandle_SelectedIndexChanged);
            // 
            // listBoxSex1
            // 
            this.listBoxSex1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxSex1.Enabled = false;
            this.listBoxSex1.FormattingEnabled = true;
            this.listBoxSex1.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Male",
            "Female"});
            this.listBoxSex1.Location = new System.Drawing.Point(244, 577);
            this.listBoxSex1.Name = "listBoxSex1";
            this.listBoxSex1.Size = new System.Drawing.Size(100, 21);
            this.listBoxSex1.TabIndex = 24;
            // 
            // listBoxBearAge1
            // 
            this.listBoxBearAge1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearAge1.Enabled = false;
            this.listBoxBearAge1.FormattingEnabled = true;
            this.listBoxBearAge1.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Adult",
            "Subadult"});
            this.listBoxBearAge1.Location = new System.Drawing.Point(395, 578);
            this.listBoxBearAge1.Name = "listBoxBearAge1";
            this.listBoxBearAge1.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearAge1.TabIndex = 28;
            // 
            // listBoxFamilyGroup
            // 
            this.listBoxFamilyGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxFamilyGroup.Enabled = false;
            this.listBoxFamilyGroup.FormattingEnabled = true;
            this.listBoxFamilyGroup.Items.AddRange(new object[] {
            "N/A",
            "No",
            "Yes"});
            this.listBoxFamilyGroup.Location = new System.Drawing.Point(637, 574);
            this.listBoxFamilyGroup.Name = "listBoxFamilyGroup";
            this.listBoxFamilyGroup.Size = new System.Drawing.Size(100, 21);
            this.listBoxFamilyGroup.TabIndex = 32;
            // 
            // listBoxBearAge2
            // 
            this.listBoxBearAge2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearAge2.Enabled = false;
            this.listBoxBearAge2.FormattingEnabled = true;
            this.listBoxBearAge2.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Adult",
            "Subadult"});
            this.listBoxBearAge2.Location = new System.Drawing.Point(395, 606);
            this.listBoxBearAge2.Name = "listBoxBearAge2";
            this.listBoxBearAge2.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearAge2.TabIndex = 29;
            // 
            // listBoxBearAge3
            // 
            this.listBoxBearAge3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearAge3.Enabled = false;
            this.listBoxBearAge3.FormattingEnabled = true;
            this.listBoxBearAge3.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Adult",
            "Subadult"});
            this.listBoxBearAge3.Location = new System.Drawing.Point(395, 634);
            this.listBoxBearAge3.Name = "listBoxBearAge3";
            this.listBoxBearAge3.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearAge3.TabIndex = 30;
            // 
            // listBoxBearAge4
            // 
            this.listBoxBearAge4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearAge4.Enabled = false;
            this.listBoxBearAge4.FormattingEnabled = true;
            this.listBoxBearAge4.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Adult",
            "Subadult"});
            this.listBoxBearAge4.Location = new System.Drawing.Point(395, 660);
            this.listBoxBearAge4.Name = "listBoxBearAge4";
            this.listBoxBearAge4.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearAge4.TabIndex = 31;
            // 
            // listBoxSex2
            // 
            this.listBoxSex2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxSex2.Enabled = false;
            this.listBoxSex2.FormattingEnabled = true;
            this.listBoxSex2.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Male",
            "Female"});
            this.listBoxSex2.Location = new System.Drawing.Point(244, 606);
            this.listBoxSex2.Name = "listBoxSex2";
            this.listBoxSex2.Size = new System.Drawing.Size(100, 21);
            this.listBoxSex2.TabIndex = 25;
            // 
            // listBoxSex3
            // 
            this.listBoxSex3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxSex3.Enabled = false;
            this.listBoxSex3.FormattingEnabled = true;
            this.listBoxSex3.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Male",
            "Female"});
            this.listBoxSex3.Location = new System.Drawing.Point(244, 633);
            this.listBoxSex3.Name = "listBoxSex3";
            this.listBoxSex3.Size = new System.Drawing.Size(100, 21);
            this.listBoxSex3.TabIndex = 26;
            // 
            // listBoxSex4
            // 
            this.listBoxSex4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxSex4.Enabled = false;
            this.listBoxSex4.FormattingEnabled = true;
            this.listBoxSex4.Items.AddRange(new object[] {
            "N/A",
            "Unknown",
            "Male",
            "Female"});
            this.listBoxSex4.Location = new System.Drawing.Point(244, 660);
            this.listBoxSex4.Name = "listBoxSex4";
            this.listBoxSex4.Size = new System.Drawing.Size(100, 21);
            this.listBoxSex4.TabIndex = 27;
            // 
            // listBoxBearSpecies
            // 
            this.listBoxBearSpecies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearSpecies.Enabled = false;
            this.listBoxBearSpecies.FormattingEnabled = true;
            this.listBoxBearSpecies.Items.AddRange(new object[] {
            "Black Bear",
            "Grizzly Bear"});
            this.listBoxBearSpecies.Location = new System.Drawing.Point(93, 606);
            this.listBoxBearSpecies.Name = "listBoxBearSpecies";
            this.listBoxBearSpecies.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearSpecies.TabIndex = 23;
            // 
            // listBoxBearFishInteraction
            // 
            this.listBoxBearFishInteraction.AutoCompleteCustomSource.AddRange(new string[] {
            "N/A",
            "No",
            "Yes"});
            this.listBoxBearFishInteraction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxBearFishInteraction.Enabled = false;
            this.listBoxBearFishInteraction.FormattingEnabled = true;
            this.listBoxBearFishInteraction.Items.AddRange(new object[] {
            "N/A",
            "No",
            "Yes"});
            this.listBoxBearFishInteraction.Location = new System.Drawing.Point(915, 580);
            this.listBoxBearFishInteraction.Name = "listBoxBearFishInteraction";
            this.listBoxBearFishInteraction.Size = new System.Drawing.Size(100, 21);
            this.listBoxBearFishInteraction.TabIndex = 35;
            // 
            // listBoxSnagAvoidance
            // 
            this.listBoxSnagAvoidance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxSnagAvoidance.Enabled = false;
            this.listBoxSnagAvoidance.FormattingEnabled = true;
            this.listBoxSnagAvoidance.Items.AddRange(new object[] {
            "N/A",
            "No",
            "Yes"});
            this.listBoxSnagAvoidance.Location = new System.Drawing.Point(915, 607);
            this.listBoxSnagAvoidance.Name = "listBoxSnagAvoidance";
            this.listBoxSnagAvoidance.Size = new System.Drawing.Size(100, 21);
            this.listBoxSnagAvoidance.TabIndex = 36;
            // 
            // listBoxCamAwareness
            // 
            this.listBoxCamAwareness.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxCamAwareness.Enabled = false;
            this.listBoxCamAwareness.FormattingEnabled = true;
            this.listBoxCamAwareness.Items.AddRange(new object[] {
            "N/A",
            "No",
            "Yes"});
            this.listBoxCamAwareness.Location = new System.Drawing.Point(915, 659);
            this.listBoxCamAwareness.Name = "listBoxCamAwareness";
            this.listBoxCamAwareness.Size = new System.Drawing.Size(100, 21);
            this.listBoxCamAwareness.TabIndex = 38;
            // 
            // listBoxOtherAnimal
            // 
            this.listBoxOtherAnimal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxOtherAnimal.FormattingEnabled = true;
            this.listBoxOtherAnimal.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.listBoxOtherAnimal.Location = new System.Drawing.Point(1102, 518);
            this.listBoxOtherAnimal.Name = "listBoxOtherAnimal";
            this.listBoxOtherAnimal.Size = new System.Drawing.Size(100, 21);
            this.listBoxOtherAnimal.TabIndex = 20;
            this.listBoxOtherAnimal.SelectedIndexChanged += new System.EventHandler(this.listBoxOtherAnimal_SelectedIndexChanged_1);
            // 
            // VideoInputMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 707);
            this.Controls.Add(this.listBoxOtherAnimal);
            this.Controls.Add(this.listBoxCamAwareness);
            this.Controls.Add(this.listBoxSnagAvoidance);
            this.Controls.Add(this.listBoxBearFishInteraction);
            this.Controls.Add(this.listBoxBearSpecies);
            this.Controls.Add(this.listBoxSex4);
            this.Controls.Add(this.listBoxSex3);
            this.Controls.Add(this.listBoxSex2);
            this.Controls.Add(this.listBoxBearAge4);
            this.Controls.Add(this.listBoxBearAge3);
            this.Controls.Add(this.listBoxBearAge2);
            this.Controls.Add(this.listBoxFamilyGroup);
            this.Controls.Add(this.listBoxBearAge1);
            this.Controls.Add(this.listBoxSex1);
            this.Controls.Add(this.listBoxCamHandle);
            this.Controls.Add(this.txtTargetIssues);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtVerification);
            this.Controls.Add(this.lblVerification);
            this.Controls.Add(this.txtGeneralComments);
            this.Controls.Add(this.lblGeneralComm);
            this.Controls.Add(this.txtCamAwarenessComm);
            this.Controls.Add(this.lblCamAwarenessComm);
            this.Controls.Add(this.lblCamAwareness);
            this.Controls.Add(this.txtSnagAvoidanceComments);
            this.Controls.Add(this.lblSnagAvoidanceComments);
            this.Controls.Add(this.lblSnagAvoidance);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblBearFishInteraction);
            this.Controls.Add(this.txtBoxYearlingCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCOYCount);
            this.Controls.Add(this.lblCOYCount);
            this.Controls.Add(this.lblFamilyGroup);
            this.Controls.Add(this.lblBearAge4);
            this.Controls.Add(this.lblBearAge3);
            this.Controls.Add(this.lblBearAge2);
            this.Controls.Add(this.lblBearAge1);
            this.Controls.Add(this.lblSex4);
            this.Controls.Add(this.lblSex3);
            this.Controls.Add(this.lblBearSex2);
            this.Controls.Add(this.lblBearSex1);
            this.Controls.Add(this.lblBearSpecies);
            this.Controls.Add(this.txtBearCount);
            this.Controls.Add(this.lblBearCount);
            this.Controls.Add(this.txtOtherAnimSppName);
            this.Controls.Add(this.lblOtherAnimalspeciesName);
            this.Controls.Add(this.lblOtherAnimal);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.txtHumanComments);
            this.Controls.Add(this.lblHumanComments);
            this.Controls.Add(this.txtTotalOtherHumans);
            this.Controls.Add(this.lblTotalOtherHumans);
            this.Controls.Add(this.txtTotalHumansOnPlatform);
            this.Controls.Add(this.lblTotalHumansOnPlatform);
            this.Controls.Add(this.txtMaxHumansFishingFromShore);
            this.Controls.Add(this.lblMaxHumanFishFromShore);
            this.Controls.Add(this.txtMaxNonDriftBoats);
            this.Controls.Add(this.lblMaxNonDriftboats);
            this.Controls.Add(this.txtMaxHumansInNonDriftBoat);
            this.Controls.Add(this.lblMacHumansInNonDriftBoats);
            this.Controls.Add(this.txtMaxDriftBoats);
            this.Controls.Add(this.lblMaxDriftBoats);
            this.Controls.Add(this.txtMaxHumansDrifting);
            this.Controls.Add(this.lblMaxHumansDrifting);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtHumanCount);
            this.Controls.Add(this.lblHumanCount);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblVidDuration);
            this.Controls.Add(this.txtVideoDuration);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTimeAtVideoStart);
            this.Controls.Add(this.lblvideoDate);
            this.Controls.Add(this.txtVideoDate);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.lblRevisitDate);
            this.Controls.Add(this.txtRevisitDate);
            this.Controls.Add(this.lnlSiteID);
            this.Controls.Add(this.txtSiteID);
            this.Controls.Add(this.lblSiteNumber);
            this.Controls.Add(this.txtSiteNumber);
            this.Controls.Add(this.lblSiteName);
            this.Controls.Add(this.txtSiteName);
            this.Controls.Add(this.lblInitials);
            this.Controls.Add(this.txtInitials);
            this.Controls.Add(this.btnEnterAndNextVideo);
            this.Controls.Add(this.lblCurrDirTxt);
            this.Controls.Add(this.trackBarVideoTracking);
            this.Controls.Add(this.wmpMainPlayer);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1287, 800);
            this.MinimumSize = new System.Drawing.Size(1287, 736);
            this.Name = "VideoInputMenu";
            this.Text = "Camera Trap Data Entry";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoInputMenu_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVideoTracking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wmpMainPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer wmpMainPlayer;
        private System.Windows.Forms.TrackBar trackBarVideoTracking;
        private System.Windows.Forms.Label lblCurrDirTxt;
        private System.Windows.Forms.Button btnEnterAndNextVideo;
        private System.Windows.Forms.TextBox txtInitials;
        private System.Windows.Forms.Label lblInitials;
        private System.Windows.Forms.Label lblSiteName;
        private System.Windows.Forms.TextBox txtSiteName;
        private System.Windows.Forms.Label lnlSiteID;
        private System.Windows.Forms.TextBox txtSiteID;
        private System.Windows.Forms.Label lblSiteNumber;
        private System.Windows.Forms.TextBox txtSiteNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTimeAtVideoStart;
        private System.Windows.Forms.Label lblvideoDate;
        private System.Windows.Forms.TextBox txtVideoDate;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label lblRevisitDate;
        private System.Windows.Forms.TextBox txtRevisitDate;
        private System.Windows.Forms.Label lblVidDuration;
        private System.Windows.Forms.TextBox txtVideoDuration;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblHumanCount;
        private System.Windows.Forms.TextBox txtHumanCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMaxHumansDrifting;
        private System.Windows.Forms.Label lblMaxHumansDrifting;
        private System.Windows.Forms.TextBox txtMaxDriftBoats;
        private System.Windows.Forms.Label lblMaxDriftBoats;
        private System.Windows.Forms.TextBox txtMaxHumansInNonDriftBoat;
        private System.Windows.Forms.Label lblMacHumansInNonDriftBoats;
        private System.Windows.Forms.TextBox txtMaxNonDriftBoats;
        private System.Windows.Forms.Label lblMaxNonDriftboats;
        private System.Windows.Forms.TextBox txtMaxHumansFishingFromShore;
        private System.Windows.Forms.Label lblMaxHumanFishFromShore;
        private System.Windows.Forms.TextBox txtTotalHumansOnPlatform;
        private System.Windows.Forms.Label lblTotalHumansOnPlatform;
        private System.Windows.Forms.ToolTip toolTipLabels;
        private System.Windows.Forms.TextBox txtHumanComments;
        private System.Windows.Forms.Label lblHumanComments;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblOtherAnimal;
        private System.Windows.Forms.TextBox txtOtherAnimSppName;
        private System.Windows.Forms.Label lblOtherAnimalspeciesName;
        private System.Windows.Forms.TextBox txtBearCount;
        private System.Windows.Forms.Label lblBearCount;
        private System.Windows.Forms.Label lblBearSpecies;
        private System.Windows.Forms.Label lblBearSex1;
        private System.Windows.Forms.Label lblBearSex2;
        private System.Windows.Forms.Label lblSex4;
        private System.Windows.Forms.Label lblSex3;
        private System.Windows.Forms.Label lblBearAge4;
        private System.Windows.Forms.Label lblBearAge3;
        private System.Windows.Forms.Label lblBearAge2;
        private System.Windows.Forms.Label lblBearAge1;
        private System.Windows.Forms.Label lblFamilyGroup;
        private System.Windows.Forms.Label lblTotalOtherHumans;
        private System.Windows.Forms.TextBox txtTotalOtherHumans;
        private System.Windows.Forms.TextBox txtCOYCount;
        private System.Windows.Forms.Label lblCOYCount;
        private System.Windows.Forms.TextBox txtBoxYearlingCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBearFishInteraction;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblSnagAvoidance;
        private System.Windows.Forms.TextBox txtSnagAvoidanceComments;
        private System.Windows.Forms.Label lblSnagAvoidanceComments;
        private System.Windows.Forms.TextBox txtCamAwarenessComm;
        private System.Windows.Forms.Label lblCamAwarenessComm;
        private System.Windows.Forms.Label lblCamAwareness;
        private System.Windows.Forms.TextBox txtGeneralComments;
        private System.Windows.Forms.Label lblGeneralComm;
        private System.Windows.Forms.TextBox txtVerification;
        private System.Windows.Forms.Label lblVerification;
        private System.Windows.Forms.Timer timerVideo;
        private System.Windows.Forms.TextBox txtTargetIssues;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox listBoxCamHandle;
        private System.Windows.Forms.ComboBox listBoxSex1;
        private System.Windows.Forms.ComboBox listBoxBearAge1;
        private System.Windows.Forms.ComboBox listBoxFamilyGroup;
        private System.Windows.Forms.ComboBox listBoxBearAge2;
        private System.Windows.Forms.ComboBox listBoxBearAge3;
        private System.Windows.Forms.ComboBox listBoxBearAge4;
        private System.Windows.Forms.ComboBox listBoxSex2;
        private System.Windows.Forms.ComboBox listBoxSex3;
        private System.Windows.Forms.ComboBox listBoxSex4;
        private System.Windows.Forms.ComboBox listBoxBearSpecies;
        private System.Windows.Forms.ComboBox listBoxBearFishInteraction;
        private System.Windows.Forms.ComboBox listBoxSnagAvoidance;
        private System.Windows.Forms.ComboBox listBoxCamAwareness;
        private System.Windows.Forms.ComboBox listBoxOtherAnimal;
    }
}